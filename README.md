lily-repl
=========

This package provides a simple repl for Lily. The repl can print the result of
simple expressions as well as parse Lily statements. If the last command given
was incomplete, the prompt is changed to reflect that.

The repl file is small because the heavy lifting is done by `spawni` which
creates an interpreter. The repl works by feeding the sub interpreter and
returning the result.

This can be installed using Lily's `garden` via:

`garden install FascinatedBox/lily-repl`
